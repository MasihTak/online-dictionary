# Online Dictionary

Online English dictionary along with an image and audio pronunciation.

A Vue project to explore the "provide/inject" API in Vue 3


# Sponsors
[![JetBrains Logo](https://masihtak.com/portfolio/assets/img/sponsors/jetbrains.svg)](https://www.jetbrains.com/?from=https://gitlab.com/MasihTak/vue-vite-scaffolding)
[![BitNinja Logo](https://masihtak.com/portfolio/assets/img/sponsors/bitninja.png)](https://bitninja.io?from=https://gitlab.com/MasihTak/vue-vite-scaffolding)
<a href="https://bitdefender.com/?from=https://gitlab.com/MasihTak/vue-vite-scaffolding"> <img src="https://masihtak.com/portfolio/assets/img/sponsors/bitdefender.jpg" alt="Bitdefender " width="400"/> </a>
