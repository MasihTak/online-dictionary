import {createRouter, createWebHistory} from 'vue-router';
import HomeView from '@/views/HomeView.vue';
import DefinitionView from "@/views/DefinitionView.vue";

const routes = [
    {
        path: '/',
        name: 'Home',
        component: HomeView,
    },
    {
        path: '/definition/:word',
        name: 'Definition',
        component: DefinitionView,
    },
];

const router = createRouter({
    history: createWebHistory(),
    routes,
});

export default router;
